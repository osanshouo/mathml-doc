pub mod variant;
pub mod token;
pub mod lexer;
pub mod ast;
pub mod parse;
pub mod mathml;
pub mod error;

/// LaTeX テキストを MathML に置き換える.
/// 
/// ただし `<math></math>` という全体の囲みは含まない.
pub fn latex_to_mathml(latex: &str) -> Result<String, error::LatexError> {
    let l = lexer::Lexer::new(latex);
    let mut p = parse::Parser::new(l);
    let nodes = p.parse()?;

    let mathml = nodes.iter()
        .map(|node| format!("{}", node))
        .collect::<String>();
    
    Ok(mathml)
}

#[cfg(test)]
mod tests {
    use super::latex_to_mathml;

    #[test]
    fn it_works() {
        let problems = vec![
            (r"0",      "<mn>0</mn>"),
            (r"3.14",   "<mn>3.14</mn>"),
            (r"x",      "<mi>x</mi>"),
            (r"\alpha", "<mi>α</mi>"),
            (r"\phi / \varphi", "<mi>ϕ</mi><mo>/</mo><mi>φ</mi>"),
            (r"x = 3 + \alpha", "<mi>x</mi><mo>=</mo><mn>3</mn><mo>+</mo><mi>α</mi>"),
            (r"\sin x", "<mo>sin</mo><mi>x</mi>"),
            (r"\sqrt 2", "<msqrt><mn>2</mn></msqrt>"),
            (r"\sqrt{x + 2}", "<msqrt><mrow><mi>x</mi><mo>+</mo><mn>2</mn></mrow></msqrt>"),
            (r"\sqrt[3]{x}", "<mroot><mi>x</mi><mn>3</mn></mroot>"),
            (r"\frac{1}{2}", "<mfrac><mn>1</mn><mn>2</mn></mfrac>"),
        ];

        for (problem, answer) in problems.iter() {
            let mathml = latex_to_mathml(problem).unwrap();
            assert_eq!(&mathml, answer);
        }
    }
}
