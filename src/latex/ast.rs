use std::fmt;
use super::variant::Variant;

/// AST node
#[derive(Debug, Clone)]
pub enum Node {
    Number(String),
    Letter(char, Variant),
    Operator(char),
    Function(&'static str),
    Subscript(Box<Node>, Box<Node>),
    Superscript(Box<Node>, Box<Node>),
    Sqrt(Option<Box<Node>>, Box<Node>),
    Frac(Box<Node>, Box<Node>),
    Row(Vec<Node>),
    Other(String),
}

impl fmt::Display for Node {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Node::Number(number)  => write!(f, "<mn>{}</mn>", number),
            Node::Letter(letter, var)  => match var {
                Variant::Italic => write!(f, "<mi>{}</mi>", letter),
                var             => write!(f, r#"<mi mathvariant="{}">{}</mi>"#, var, letter),
            },
            Node::Operator(op)    => write!(f, "<mo>{}</mo>", op),
            Node::Function(fun)   => write!(f, "<mo>{}</mo>", fun),
            Node::Subscript(a, b) => write!(f, "<msub>{}{}</msub>", a, b),
            Node::Superscript(a, b) => write!(f, "<msup>{}{}</msup>", a, b),
            Node::Sqrt(degree, content) => match degree {
                Some(deg) => write!(f, "<mroot>{}{}</mroot>", content, deg),
                None      => write!(f, "<msqrt>{}</msqrt>", content),
            },
            Node::Frac(num, denom) => write!(f, "<mfrac>{}{}</mfrac>", num, denom),
            Node::Row(vec) => write!(f, "<mrow>{}</mrow>", 
                vec.iter().map(|node| format!("{}", node)).collect::<String>()
            ),
            _ => unimplemented!(),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::super::variant::Variant;
    use super::Node;

    #[test]
    fn node_display() {
        let problems = vec![
            (Node::Number("3.14".to_owned()), "<mn>3.14</mn>"),
            (Node::Letter('x', Variant::Italic), "<mi>x</mi>"),
            (Node::Letter('α', Variant::Italic), "<mi>α</mi>"),
            (Node::Letter('あ', Variant::Normal), r#"<mi mathvariant="normal">あ</mi>"#),
            (
                Node::Row(vec![ Node::Operator('+'), Node::Operator('-') ]), 
                r"<mrow><mo>+</mo><mo>-</mo></mrow>"
            ),
        ];
        for (problem, answer) in problems.iter() {
            assert_eq!(&format!("{}", problem), answer);
        }
    }
}
