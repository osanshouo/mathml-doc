use super::{
    token::Token, 
    lexer::Lexer,
    ast::Node,
    error::LatexError,
};

#[derive(Debug, Clone)]
pub struct Parser<'a> {
    l: Lexer<'a>,
    cur_token: Token,
    peek_token: Token,
}
impl<'a> Parser<'a> {
    pub fn new(l: Lexer<'a>) -> Self {
        let mut p = Parser { 
            l, 
            cur_token: Token::Illegal('\u{0}'),
            peek_token: Token::Illegal('\u{0}'),
        };
        p.next_token();
        p.next_token();
        p
    }

    fn next_token(&mut self) {
        self.cur_token = self.peek_token.clone();
        self.peek_token = self.l.next_token();
    }

    fn cur_token_is(&self, expected_token: &Token) -> bool {
        &self.cur_token == expected_token
    }

    fn peek_token_is(&self, expected_token: Token) -> bool {
        self.peek_token == expected_token
    }

    pub(crate) fn expect_peek(&mut self, expected_token: Token) -> Result<(), LatexError> {
        if self.peek_token_is(expected_token.clone()) {
            self.next_token();
            Ok(())
        } else {
            Err(LatexError::UnexpectedToken {
                expected: expected_token,
                got: self.peek_token.clone()
            })
        }
    }

    pub fn parse(&mut self) -> Result<Vec<Node>, LatexError> {
        let mut nodes = Vec::new();

        while !self.cur_token_is(&Token::EOF) {
            nodes.push(
                self.parse_node()?
            );
            self.next_token();
        }

        Ok(nodes)
    }

    fn parse_node(&mut self) -> Result<Node, LatexError> {
        match &self.cur_token {
            Token::Number(number) => Ok(Node::Number(number.clone())),
            Token::Letter(x, v) => Ok(Node::Letter(*x, *v)),
            Token::Operator(op)   => Ok(Node::Operator(*op)),
            Token::Function(fun)  => Ok(Node::Function(fun)),
            Token::Sqrt => {
                self.next_token();
                let degree = if self.cur_token_is(&Token::LParen("[")) {
                    let degree = self.parse_group(&Token::RParen("]"))?;
                    self.next_token();
                    Some(Box::new(degree))
                } else { None };
                let content = self.parse_node()?;
                Ok(Node::Sqrt(degree, Box::new(content)))
            },
            Token::Frac => {
                self.next_token();
                let numerator = self.parse_node()?;
                self.next_token();
                let denominator = self.parse_node()?;
                Ok(Node::Frac(Box::new(numerator), Box::new(denominator)))
            },
            Token::LBrace => self.parse_group(&Token::RBrace),
            _ => unimplemented!(),
        }
    }

    fn parse_group(&mut self, end_token: &Token) -> Result<Node, LatexError> {
        self.next_token();
        let mut nodes = Vec::new();

        while !self.cur_token_is(end_token) {
            nodes.push(
                self.parse_node()?
            );
            self.next_token();
        }

        if nodes.len() == 1 {
            let node = nodes.into_iter().nth(0).unwrap();
            Ok(node)
        } else {
            Ok(Node::Row(nodes))
        }
    }
}
