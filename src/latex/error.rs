use super::token::Token;

#[derive(Debug, Clone)]
pub enum LatexError {
    UnexpectedToken {
        expected: Token, got: Token,
    }
}
