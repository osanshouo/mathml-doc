# mathml-doc

`mathml-doc` converts latex math equations in your Rust docs to MathML. This enable to embed math equations in your Rust docs.

- Mozilla Firefox and Apple Safari support MathML.
- Google Chrome and Microsoft Edge (both EdgeHTML and WebKit) can not render MathML at present, but implementation for WebKit is in progress.


# Supported LaTeX commands

- Numbers, e.g. `0`, `3.14`, ...
- ASCII or Greek letters, e.g. `x`, `\alpha`, `\pi`, ...
  - Non-ASCII inputs, e.g. `α` and `あ`, are not supported. Please use `\alpha` instead.
- Binary relations, e.g. `=`, `>`, `<`, ...
- Binary operations, e.g. `+`. `-`, `*`, `/`, ...

## Unsupported LaTeX commands

- New line `\\`
- `\begin{..}` and `\end{..}`
  - So matrix is currently lacked. It is one of the most important features, so I'll try to implement it when I have enough time.
- Equation alignment `&`
